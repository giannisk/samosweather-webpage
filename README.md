# SamosWeather - Webpage Design

This webpage was created about three years ago. It does no longer follow the latest design trends.

![Preview](/img/preview.png)

Based on [Materialize](http://materializecss.com/) and the [Starter Template](http://materializecss.com/getting-started.html#templates).

## License

Final work is made available under the [MIT License](LICENSE).

Materialize is available the [MIT License](https://github.com/Dogfalo/materialize/blob/master/LICENSE).
